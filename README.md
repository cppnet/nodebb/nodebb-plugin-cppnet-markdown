# NodeBB Markdown Parser

This NodeBB plugin is a parser that allows users to write posts using [Markdown](http://commonmark.org/). It is derived from the `nodebb-plugin-markdown` and extended for the [c-plusplus.net](https://www.c-plusplus.net/) community.

To customise options for the parser, please consult the "Markdown" page in the administration panel, under the "Plugins" heading.

## Installation

`npm install nodebb-plugin-cppnet-markdown`
