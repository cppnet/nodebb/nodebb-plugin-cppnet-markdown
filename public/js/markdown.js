'use strict';

export function capturePaste(targetEl) {
	targetEl.on('paste', function (e) {
		var triggers = [/^>\s*/, /^\s*\*\s+/, /^\s*\d+\.\s+/, /^\s{4,}/];
		var start = e.target.selectionStart;
		var line = getLine(targetEl.val(), start);

		var trigger = triggers.reduce(function (regexp, cur) {
			if (regexp) {
				return regexp;
			}

			return cur.test(line) ? cur : false;
		}, false);

		var prefix = line.match(trigger);
		if (prefix) {
			prefix = prefix.shift();

			var payload = e.originalEvent.clipboardData.getData('text');
			var fixed = payload.replace(/^/gm, prefix).slice(prefix.length);

			setTimeout(function () {
				var replacement = targetEl.val().slice(0, start) + fixed + targetEl.val().slice(start + payload.length);
				targetEl.val(replacement);
			}, 0);
		}
	});

	function getLine(text, selectionStart) {
		// Break apart into lines, return the line the cursor is in
		var lines = text.split('\n');

		return lines.reduce(function (memo, cur) {
			if (typeof memo !== 'number') {
				return memo;
			} if (selectionStart > (memo + cur.length)) {
				return memo + cur.length + 1;
			}

			return cur;
		}, 0);
	}
};

export function prepareFormattingTools() {
	require([
		'composer/formatting',
		'composer/controls',
		'translator',
	], function (formatting, controls, translator) {
		if (formatting && controls) {
			translator.getTranslations(window.config.userLang || window.config.defaultLang, 'markdown', function (strings) {
				// used for h1,h2...h6
				function formatHeading(heading, textarea, selectionStart, selectionEnd) {
					if (selectionStart === selectionEnd) {
						controls.insertIntoTextarea(textarea, `${heading} ${strings.heading}`);

						controls.updateTextareaSelection(
							textarea, selectionStart + heading.length + 1, selectionStart + strings.heading.length + heading.length + 1
						);
					} else {
						const selectedText = $(textarea).val().substring(selectionStart, selectionEnd);
						const newText = `${heading} ${selectedText}`;
						controls.replaceSelectionInTextareaWith(textarea, newText);
						controls.updateTextareaSelection(textarea, selectionStart + (heading.length + 1), selectionEnd + (newText.length - selectedText.length));
					}
				}


				formatting.addButtonDispatch('bold', function (textarea, selectionStart, selectionEnd) {
					if (selectionStart === selectionEnd) {
						var block = controls.getBlockData(textarea, '**', selectionStart);

						if (block.in && block.atEnd) {
							// At end of bolded string, move cursor past delimiters
							controls.updateTextareaSelection(textarea, selectionStart + 2, selectionStart + 2);
						} else {
							controls.insertIntoTextarea(textarea, '**' + strings.bold + '**');
							controls.updateTextareaSelection(
								textarea, selectionStart + 2, selectionStart + strings.bold.length + 2
							);
						}
					} else {
						var wrapDelta = controls.wrapSelectionInTextareaWith(textarea, '**');
						controls.updateTextareaSelection(
							textarea, selectionStart + 2 + wrapDelta[0], selectionEnd + 2 - wrapDelta[1]
						);
					}
				});

				formatting.addButtonDispatch('italic', function (textarea, selectionStart, selectionEnd) {
					if (selectionStart === selectionEnd) {
						var block = controls.getBlockData(textarea, '*', selectionStart);

						if (block.in && block.atEnd) {
							// At end of italicised string, move cursor past delimiters
							controls.updateTextareaSelection(textarea, selectionStart + 1, selectionStart + 1);
						} else {
							controls.insertIntoTextarea(textarea, '*' + strings.italic + '*');
							controls.updateTextareaSelection(
								textarea, selectionStart + 1, selectionStart + strings.italic.length + 1
							);
						}
					} else {
						var wrapDelta = controls.wrapSelectionInTextareaWith(textarea, '*');
						controls.updateTextareaSelection(
							textarea, selectionStart + 1 + wrapDelta[0], selectionEnd + 1 - wrapDelta[1]
						);
					}
				});

				[1, 2, 3, 4, 5, 6].forEach((size) => {
					formatting.addButtonDispatch(`heading${size}`, function (textarea, selectionStart, selectionEnd) {
						formatHeading(new Array(size).fill('#').join(''), textarea, selectionStart, selectionEnd);
					});
				})

				formatting.addButtonDispatch('list', function (textarea, selectionStart, selectionEnd) {
					if (selectionStart === selectionEnd) {
						controls.insertIntoTextarea(textarea, '\n* ' + strings['list-item']);

						// Highlight "list item"
						controls.updateTextareaSelection(
							textarea, selectionStart + 3, selectionStart + strings['list-item'].length + 3
						);
					} else {
						const selectedText = $(textarea).val().substring(selectionStart, selectionEnd);
						const newText = '* ' + selectedText.split('\n').join('\n* ');
						controls.replaceSelectionInTextareaWith(textarea, newText);
						controls.updateTextareaSelection(textarea, selectionStart + 2, selectionEnd + (newText.length - selectedText.length));
					}
				});

				formatting.addButtonDispatch('strikethrough', function (textarea, selectionStart, selectionEnd) {
					if (selectionStart === selectionEnd) {
						var block = controls.getBlockData(textarea, '~~', selectionStart);

						if (block.in && block.atEnd) {
							// At end of bolded string, move cursor past delimiters
							controls.updateTextareaSelection(textarea, selectionStart + 2, selectionStart + 2);
						} else {
							controls.insertIntoTextarea(textarea, '~~' + strings['strikethrough-text'] + '~~');
							controls.updateTextareaSelection(
								textarea, selectionStart + 2, selectionEnd + strings['strikethrough-text'].length + 2
							);
						}
					} else {
						var wrapDelta = controls.wrapSelectionInTextareaWith(textarea, '~~', '~~');
						controls.updateTextareaSelection(
							textarea, selectionStart + 2 + wrapDelta[0], selectionEnd + 2 - wrapDelta[1]
						);
					}
				});

				formatting.addButtonDispatch('code', function (textarea, selectionStart, selectionEnd) {
					const codeSelection = document.getElementById('MarkdownCodeHighlightLanguageSelection');
					const language = (codeSelection && codeSelection.selectedOptions.length > 0 && codeSelection.selectedOptions[0].value) || '';
					if (selectionStart === selectionEnd) {
						controls.insertIntoTextarea(textarea, '```' + language + '\n' + strings['code-text'] + '\n```');
						controls.updateTextareaSelection(
							textarea, selectionStart + 4 + language.length, selectionEnd + strings['code-text'].length + 4 + language.length
						);
					} else {
						var wrapDelta = controls.wrapSelectionInTextareaWith(textarea, '```' + language + '\n', '\n```');
						controls.updateTextareaSelection(
							textarea, selectionStart + 4 + language.length + wrapDelta[0], selectionEnd + 4 - wrapDelta[1] + language.length
						);
					}
				});

				formatting.addButtonDispatch('link', function (textarea, selectionStart, selectionEnd) {
					if (selectionStart === selectionEnd) {
						controls.insertIntoTextarea(textarea, '[' + strings['link-text'] + '](' + strings['link-url'] + ')');
						controls.updateTextareaSelection(
							textarea,
							selectionStart + strings['link-text'].length + 3,
							selectionEnd + strings['link-text'].length + strings['link-url'].length + 3
						);
					} else {
						var wrapDelta = controls.wrapSelectionInTextareaWith(textarea, '[', '](' + strings['link-url'] + ')');
						controls.updateTextareaSelection(
							textarea, selectionEnd + 3 - wrapDelta[1], selectionEnd + strings['link-url'].length + 3 - wrapDelta[1]
						);
					}
				});

				formatting.addButtonDispatch('picture-o', function (textarea, selectionStart, selectionEnd) {
					if (selectionStart === selectionEnd) {
						controls.insertIntoTextarea(textarea, '![' + strings['picture-text'] + '](' + strings['picture-url'] + ')');
						controls.updateTextareaSelection(
							textarea,
							selectionStart + strings['picture-text'].length + 4,
							selectionEnd + strings['picture-text'].length + strings['picture-url'].length + 4
						);
					} else {
						var wrapDelta = controls.wrapSelectionInTextareaWith(textarea, '![', '](' + strings['picture-url'] + ')');
						controls.updateTextareaSelection(
							textarea, selectionEnd + 4 - wrapDelta[1], selectionEnd + strings['picture-url'].length + 4 - wrapDelta[1]
						);
					}
				});
			});
		}
	});
};

export function markExternalLinks() {
	if (!config.markdown.externalMark) {
		return;
	}

	const anchorEls = document.querySelectorAll('[component="post/content"] a');
	anchorEls.forEach((anchorEl) => {
		// Do nothing if the anchor contains only an image
		if (anchorEl.childElementCount === 1 && anchorEl.querySelector('img') && !anchorEl.text) {
			return;
		}

		// Otherwise, mark external links with icon
		const parsed = new URL(anchorEl.href, document.location.href);
		if (parsed.host != document.location.host) {
			const iconEl = document.createElement('i');
			iconEl.classList.add('fa', 'fa-external-link', 'small');
			anchorEl.append(' ', iconEl);
		}
	})
}

export function enhanceCheckbox(ev, data) {
	if (!data.posts && !data.post) {
		return;
	} if (data.hasOwnProperty('post')) {
		data.posts = [data.post];
	}

	var disable;
	var checkboxEls;
	data.posts.forEach(function (post) {
		disable = !post.display_edit_tools;
		checkboxEls = $('.posts li[data-pid="' + post.pid + '"] .content div.plugin-markdown input[type="checkbox"]');

		checkboxEls.on('click', function (e) {
			if (disable) {
				// Find the post's checkboxes in DOM and make them readonly
				e.preventDefault();
			}

			// Otherwise, edit the post to reflect state change
			var _this = this;
			var pid = $(this).parents('li[data-pid]').attr('data-pid');
			var index = $(this).parents('.content').find('input[type="checkbox"]').toArray()
				.reduce(function (memo, cur, index) {
					if (cur === _this) {
						memo = index;
					}

					return memo;
				}, null);

			socket.emit('plugins.markdown.checkbox.edit', {
				pid: pid,
				index: index,
				state: $(_this).prop('checked'),
			});
		});
	});
};

export function highlight(data) {
	if (data instanceof jQuery.Event) {
		processHighlight($(data.data.selector));
	} else {
		processHighlight(data);
	}
};

const aliasMap = new Map();

export function buildAliasMap() {
	if (window.hljs) {
		const hljs = window.hljs;
		hljs.listLanguages().forEach((language) => {
			const { aliases } = hljs.getLanguage(language);
			if (aliases && Array.isArray(aliases)) {
				aliases.forEach((alias) => {
					aliasMap.set(alias, language);
				});
			}

			aliasMap.set(language, language);
		});
	}
}

async function processHighlight(elements) {
	if (parseInt(config.markdown.highlight, 10)) {
		const hljs = window.hljs;
		if (!hljs) {
			console.debug(`[plugins/markdown] Tryin to highlight without initializing hljs`);
			return;
		}

		elements.each(function (i, block) {
			const parentNode = $(block.parentNode);
			if (parentNode.hasClass('markdown-highlight')) {
				return;
			}
			parentNode.addClass('markdown-highlight');
			if (block.getBoundingClientRect().height > 350) {
				require([
					'translator',
				], function (translator) {
					translator.getTranslations(window.config.userLang || window.config.defaultLang, 'markdown', function (strings) {
						const expandBtn = $('<button' +
							' class="hover-visible position-absolute top-0 btn btn-sm btn-outline-secondary btn-code-resize"' +
							' title="' + strings['expand-collapse'] + '">'
							+ '<i class="fa fa-fw fa-expand"></i>'
							+ '</button>');

						parentNode.parent().append(expandBtn);
						expandBtn.on('click', toggleExpandCode);
					});
				});
			}

			// Default language if set in ACP
			if (!Array.prototype.some.call(block.classList, (className) => className.startsWith('language-')) && config.markdown.defaultHighlightLanguage) {
				block.classList.add(`language-${config.markdown.defaultHighlightLanguage}`);
			}

			window.hljs.highlightElement(block);

			// Check detected language against whitelist and add lines if enabled
			const classIterator = block.classList.values();
			for (const className of classIterator) {
				if (className.startsWith('language-')) {
					const language = className.split('-')[1];
					const list = config.markdown.highlightLinesLanguageList;
					if (aliasMap.has(language) && list && list.includes(aliasMap.get(language))) {
						addCodeBlockLineNumbers(block);
					}
					break;
				}
			}
		});
	}
}

export function insertCodeSelection(container) {
	const codeItem = container.find('.formatting-bar .formatting-group button[data-format="code"]').parent();
	codeItem.before('<li><select id="MarkdownCodeHighlightLanguageSelection" class="form-select form-select-sm">'
		+ '<option value="cpp" selected>C++</option>'
		+ '<option value="c">C</option>'
		+ '<option value="csharp">C#</option>'
		+ '<option value="java">Java</option>'
		+ '<option disabled>───────</option>'
		+ '<option value="">Auto</option>'
		+ '<option value="plain">Text</option>'
		+ '<option disabled>───────</option>'
		+ '<option value="delphi">Delphi</option>'
		+ '<option value="html">HTML</option>'
		+ '<option value="javascript">Javascript</option>'
		+ '<option value="php">PHP</option>'
		+ '<option value="xml">XAML</option>'
		+ '<option value="xml">XML</option>'
		+ '</select></li>');

	codeItem.on('click', '#MarkdownCodeHighlightLanguageSelection', function (event) {
		event.stopPropagation();
	});
}

function addCodeBlockLineNumbers(codeElement) {
	if (codeElement.querySelector('.line-numbers-rows')) {
		// Abort if line numbers already exists
		return;
	}

	const pre = codeElement.parentNode;
	const clsReg = /\s*\bline-numbers\b\s*/;
	if (!clsReg.test(pre.className)) {
		// Add the class 'line-numbers' to the <pre>
		pre.className += ' line-numbers';
	}

	const match = codeElement.innerText.match(/\n/g);
	const linesNum = match ? match.length + 1 : 1;

	const lines = new Array(linesNum).join('<span></span>');
	const lineNumbersWrapper = document.createElement('span');
	lineNumbersWrapper.setAttribute('aria-hidden', 'true');
	lineNumbersWrapper.className = 'line-numbers-rows';
	lineNumbersWrapper.innerHTML = lines;

	codeElement.prepend(lineNumbersWrapper);
}

function toggleExpandCode(event) {
	const target = $(event.target);
	const pre = target.siblings('pre');
	const icon = target.children('i.fa');
	if (pre.hasClass('markdown-highlight-expanded')) {
		pre.removeClass('markdown-highlight-expanded');
		icon.removeClass('fa-compress');
		icon.addClass('fa-expand');
	} else {
		pre.addClass('markdown-highlight-expanded');
		icon.removeClass('fa-expand');
		icon.addClass('fa-compress');
	}
}
